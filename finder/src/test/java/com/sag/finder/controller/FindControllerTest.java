package com.sag.finder.controller;

import com.sag.finder.model.FileType;
import com.sag.finder.model.Request;
import com.sag.finder.service.FindService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(FindController.class)
public class FindControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FindService findService;

    @InjectMocks
    private FindController findController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(findController).build();
    }

    @Test
    public void whenValidInput_findPhrase_thenReturnsStatusOK() throws Exception {
        Request request = new Request("D:/AG/finder/src/main/resources/static/", "proba", FileType.TEXT);
        when(findService.findString(request)).thenReturn(String.valueOf(request));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("http://localhost:8078/v1/finder/phrase/")
                        .characterEncoding("UTF-8")
                        .param("location", "D:/AG/finder/src/main/resources/static/")
                        .param("text", "proba")
                        .param("fileType", ".txt"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());

        verify(findService, times(1)).findString(request);
        verifyNoMoreInteractions(findService);
    }

    @Test
    public void whenMissingPhraseParameter_findPhrase_thenReturnsStatus400() throws Exception {
        Request request = new Request("D:/AG/finder/src/main/resources/static/", "proba", FileType.TEXT);
        when(findService.findString(request)).thenReturn(String.valueOf(request));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("http://localhost:8078/v1/finder/phrase/")
                        .characterEncoding("UTF-8")
                        .param("location", "D:/AG/finder/src/main/resources/static/")
                        .param("fileType", ".txt"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenMissingLocationParameter_findPhrase_thenReturnsStatus400() throws Exception {
        Request request = new Request("D:/AG/finder/src/main/resources/static/", "proba", FileType.TEXT);
        when(findService.findString(request)).thenReturn(String.valueOf(request));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("http://localhost:8078/v1/finder/phrase/")
                        .characterEncoding("UTF-8")
                        .param("text", "proba")
                        .param("fileType", ".txt"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest());
    }
}