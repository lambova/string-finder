package com.sag.finder.service;

import com.sag.finder.model.FileType;
import com.sag.finder.model.Request;
import com.sag.finder.repository.RequestRepository;
import com.sag.finder.service.impl.FindServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class FindServiceTest {

    private FindServiceImpl findService;

    @Mock
    private RequestRepository requestRepository;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
        findService = new FindServiceImpl(requestRepository);
        Mockito.when(requestRepository.save(Mockito.any(Request.class))).thenReturn(new Request());
    }

    @Test
    public void whenFindString_ShouldSaveRequest() {
        Request request = new Request(1L,"D:/AG/finder/src/main/resources/static/", "proba", FileType.TEXT, LocalDateTime.now());
        String result = findService.findString(request);

        assertNotNull(result);
        verify(requestRepository, times(1)).save(request);
        verifyNoMoreInteractions(requestRepository);
    }
}