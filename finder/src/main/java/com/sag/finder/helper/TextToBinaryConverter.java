package com.sag.finder.helper;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class TextToBinaryConverter {

//    public static void main(String[] args) {
//        String text = "\n\nprobaproba ldfja kofla;s\n\nprobalj'dajfk 'lkajdf ;a f probaproba\n\naf a proba fa s proba";
//        String filePath = "D:/AG/finder/src/main/resources/static/binfile.bin";
//        makeBinFileFromText(text, filePath);
//    }

    public static void makeBinFileFromText(String text, String filePath) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath);
             DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream)) {
            dataOutputStream.writeUTF(text);
            System.out.println("Text converted to binary file successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
