package com.sag.finder.service;

import com.sag.finder.model.Request;

public interface FindService {
    String findString(Request request);
}
