package com.sag.finder.service.impl;

import com.sag.finder.model.FileType;
import com.sag.finder.model.Request;
import com.sag.finder.repository.RequestRepository;
import com.sag.finder.service.FindService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.riversun.bigdoc.bin.BigFileSearcher;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class FindServiceImpl implements FindService {

    public static final String JSON_PATTERN = """
              {
                  location: %s
                  idx: %d
              },
            """;

    private final RequestRepository repository;

    public FindServiceImpl(RequestRepository repository) {
        this.repository = repository;
    }

    public String findString(Request request) {
        repository.save(request);       StringBuilder sb = new StringBuilder();
        sb.append("[\n");
        try {
            traverseDirectory(sb, request.getLocation(), request.getPattern(), request.getFileType());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sb.delete(sb.length() - 2, sb.length() - 1);
        sb.append("]");
        return sb.toString();
    }

    public void traverseDirectory(StringBuilder sb, String filePath, String pattern, FileType fileType) throws IOException {
        File dir = new File(filePath);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (String child : children) {
                    File childFile = new File(dir, child);
                    traverseDirectory(sb, childFile.getPath(), pattern, fileType);
                }
            }
        } else {
            List<Long> list;
            if (dir.length() >= sizeOfBigFile()) {
                list = findInBigFile(dir, pattern, fileType);
            } else {
                list = findInFileWithParser(dir, pattern, fileType);
            }
            if (fileType.equals(FileType.PLAIN)) {
                list = findTxt(dir, pattern);
            }
            addToJson(sb, dir, list);
        }
    }

    private List<Long> findInBigFile(File dir, String searchString, FileType fileType) {
        List<Long> listOfIndexes = new ArrayList<>();
        byte[] searchBytes = searchString.getBytes(StandardCharsets.UTF_8);
        if (dir.getName().endsWith(String.valueOf(fileType))) {
            File file = new File(dir.getPath());
            BigFileSearcher searcher = new BigFileSearcher();
            listOfIndexes = searcher.searchBigFile(file, searchBytes);
        }
        return listOfIndexes;
    }

    private List<Long> findInFileWithParser(File file, String searchString, FileType fileType) {
        List<Long> listOfIndexes = new ArrayList<>();
        if (file.getName().endsWith(String.valueOf(fileType))) {
            try {
                File input = new File(file.getPath());
                Document doc = Jsoup.parse(input, "UTF-8");
                String docHtml = doc.html();
                listOfIndexes = findSubstringIndexes(docHtml, searchString);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return listOfIndexes;
    }

    private List<Long> findTxt(File file, String searchString) {
        List<Long> listOfIndexes = new ArrayList<>();
        for (FileType type : FileType.values()) {
            if (file.getName().endsWith(type.toString())) {
                return listOfIndexes;
            }
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(file.getPath()))) {
            StringBuilder fileContent = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                fileContent.append(line);
            }
            listOfIndexes = findSubstringIndexes(fileContent.toString(), searchString);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfIndexes;
    }

    public List<Long> findSubstringIndexes(String text, String substring) {
        List<Long> indexes = new ArrayList<>();
        int index = 0;
        while (index < text.length()) {
            index = text.indexOf(substring, index);
            if (index == -1) {
                break;
            }
            indexes.add((long) index);
            index += substring.length();
        }
        return indexes;
    }

    private void addToJson(StringBuilder sb, File dir, List<Long> list) {
        list.forEach(b -> sb.append(String.format(JSON_PATTERN, dir.getPath(), b)));
    }

    private float sizeOfBigFile() {
        float sizeKb = 1024.0f;
        float sizeMb = sizeKb * sizeKb;
        float sizeGb = sizeMb * sizeKb;
        return sizeGb;
    }
}
