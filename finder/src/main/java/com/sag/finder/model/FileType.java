package com.sag.finder.model;

public enum FileType {
    TEXT(".txt"),
    PLAIN("plain"),
    HTML(".html"),
    XML(".xml"),
    BINARY(".bin");

    String extension;

    FileType(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public static FileType byExtension(String extension) {
        for (FileType ft : FileType.values()) {
            if (ft.getExtension().equals(extension)) {
                return ft;
            }
        }
        throw new IllegalArgumentException("This type of file is not supported.");
    }

    @Override
    public String toString() {
        switch (this) {
            case TEXT, PLAIN -> {return ".txt"; }
            case HTML -> { return ".html"; }
            case XML -> { return ".xml"; }
            case BINARY -> { return ".bin"; }
            default -> throw new IllegalArgumentException("No such type of file.");
        }
    }
}
