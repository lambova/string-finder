package com.sag.finder.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Request {

    public Request(String location, String pattern, FileType fileType) {
        this.location = location;
        this.pattern = pattern;
        this.fileType = fileType;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    String location;

    @Column
    String pattern;

    @Column
    FileType fileType;

    @Column
    LocalDateTime requestDateTime;
}
