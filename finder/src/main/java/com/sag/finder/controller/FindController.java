package com.sag.finder.controller;

import com.sag.finder.model.FileType;
import com.sag.finder.model.Request;
import com.sag.finder.service.FindService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/finder")
@Controller
public class FindController {

    private final FindService findService;

    public FindController(FindService findService) {
        this.findService = findService;
    }

    @GetMapping(value = "/phrase", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> findPhrase(@RequestParam(value="location") String location,
            @RequestParam (value="text") String pattern,
            @RequestParam (value="fileType", defaultValue = ".txt") String fileType){
        String phrase = findService.findString(new Request(location, pattern, FileType.byExtension(fileType)));
        return ResponseEntity.ok(phrase);
    }

}